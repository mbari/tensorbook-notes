

Setup Notes:

Install anaconda3 from website, latest version

pip install opencv-python
pip install opencv-contrib-pyhton
pip install pyqtgraph
conda install -c anaconda pyopengl

clone latest lcm and build from source

on HPC cluster need to also install

sudo apt install cmake
sudo apt install autopoint
sudo apt install libglib2.0-dev
sudo apt install python3-dev

and need to add local anaconda lib path to library path: 

export LD_LIBRARY_PATH=/home/hpc-cloud/anaconda3/lib/:$LD_LIBRARY_PATH

cd lcm
mkdir build
cd build
cmake ..
make
sudo make install

then copy the python modules to anaconda:

cp -rf /usr/local/lib/python3.7/site-packages/lcm ~/anaconda3/lib/python3.7/site-packages/


Test installation 

cd ~/software/lcm/examples/python
./gen_types.sh
python listener.py
(in separate terminal)
python send_message.py

