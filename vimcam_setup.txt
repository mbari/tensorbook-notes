Steps to setup vimc and vimview:

#########################################################################################################
Download and install Vimba_3_0

get the archive from here: https://www.alliedvision.com/en/products/software.html
extract and place Vimba_3_0 in the dir above the one that contains dsvimc and dsvimview such
that the make files find it at ../../../Vimba_3_0

#########################################################################################################
Build opencv 3.4.6

git clone https://github.com/opencv/opencv.git
git clone https://github.com/opencv/opencv_contrib.git
cd opencv_contrib
git checkout tags/3.4.6
cd ..
cd opencv
git checkout tags/3.4.6
mkdir build
cd build
cmake -D CMAKE_BUILD_TYPE=RELEASE \
        -D CMAKE_INSTALL_PREFIX=/usr/local/OpenCV-3.4.6 \
        -D WITH_QT=ON \
        -D WITH_OPENGL=ON \
        -D OPENCV_EXTRA_MODULES_PATH=../../opencv_contrib/modules \
        -D BUILD_TIFF=ON ..
make -j7
sudo make install

#########################################################################################################
Build dsvimlib:

cd dsvimlib
rm -rf build
mkdir build
cd build
cmake ..
make

#########################################################################################################
Build dsvimc

cd dsvimc
cd src
vi vimc.pro

(edit vimc.pro to match the system config so it finds the required includes and libs) I added a MINIROV
define to reflect this configration

cd ..
cd build
qmake ../src
make

export LD_LIBRARY_PATH=/usr/local/OpenCV-3.4.6/lib:../../dsvimlib/build:../../Vimba_3_0/VimbaCPP/DynamicLib/x86_64bit

(placed in addLibPath)

./vimc should run now

#########################################################################################################
Build dsvimview

cd dsvimview
cd src
vi vimview.pro

(edit vimview.pro to reflect the system config so it finds the required includes and libs) I made these changes

cd ..

make sure to add mbari_types and lcm-gen them, needed to add the mbt_target_candidate_t type following compile
erorrs. Just add the types as needed to fix the errors.

cd build
qmake ../src
make















